const {src, dest, watch, series} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const browserSync = require('browser-sync').create();


const destination = 'project'

//Sass Task
function scssTask(){
   return src(`sass/style.scss`, { sourcemaps: true, style: "compressed"})
          .pipe(sass())
          .pipe(postcss([cssnano()]))
          .pipe(dest(`${destination}/css`,  { sourcemaps: '.' }))
}

function browserSyncReload(cb){
    browserSync.reload();
cb();
}

// Watch Task
function watchTask(cb){
    watch(`${destination}/**/*.html`, browserSyncReload);
    watch(`sass/**/*`, browserSyncReload);
cb();
}

// BrowserSync Task
function serverSync(){
    browserSync.init({
    notify: false,
    open: false,
        server: {
            baseDir: destination
        }
    })
    watch(`sass/**/*`, scssTask);
}




exports.default = series(scssTask, watchTask, serverSync);
  

